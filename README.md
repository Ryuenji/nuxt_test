# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

### Ruby version  
+ 2.6.3(recommend rbenv)  

### System dependencies  
+ native package installation    
```
$ sudo apt install libssl-dev libsqlite3-dev libpq-dev
```

### Development instructions  
1. `$ cd ~/nuxt_test/frontend`  
2. `$ yarn install && yarn dev &`  
3. `$ cd ~/nuxt_test`  
4. `$ bundle install && rails s`  
